# MyAsset
This is a homework project in Kotlin.
In this application you can:
-	Add new employees with id, name, mobile, workplace parameters
-	Search employees by name
-	Remove employees
-	Modify user data
-	Attach devices to employees
-	Remove devices

In this application there is a profile view for employees, where you can edit employees, and there is a list with all their assets.


