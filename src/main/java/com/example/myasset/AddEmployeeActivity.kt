package com.example.myasset

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_addemployee.*

class AddEmployeeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_addemployee)
    }

    fun AddEmployeeClicked(view: View){

        val eid = employeeIdNum.text.toString()
        val ename = employeeNameText.text.toString()
        val eplace = placeText.text.toString()
        val emobile = mobileText.text.toString()

        var succeed = false

        try {
            val database = this.openOrCreateDatabase("AssetDB", Context.MODE_PRIVATE, null)
            database.execSQL("CREATE TABLE IF NOT EXISTS employees (id INTEGER(4), name VARCHAR, place VARCHAR, mobile VARCHAR)")
            if (!UserIsExist(eid.toInt())){
                var insertSQL = "INSERT INTO employees (id, name, place, mobile) VALUES ($eid, '$ename','$eplace','$emobile')"
                database.execSQL(insertSQL)
                succeed = true
            } else if (UserIsExist(eid.toInt())){
                Toast.makeText(applicationContext,"This ID is already exists choose another one",Toast.LENGTH_LONG).show()
            }
        } catch (e: Exception){
            e.printStackTrace()
        }
        if (succeed){
            val intent = Intent(applicationContext, MainActivity::class.java)
            startActivity(intent)
        }
    }

    fun UserIsExist(new_id:Int):Boolean{
        val EmployeeIdArray = ArrayList<Int>()
        val EmployeeNameArray = ArrayList<String>()
        val EmployeePlaceArray = ArrayList<String>()
        val EmployeeMobileArray = ArrayList<String>()
        try {
            val database = this.openOrCreateDatabase("AssetDB", Context.MODE_PRIVATE, null)
            database.execSQL("CREATE TABLE IF NOT EXISTS employees (id INTEGER(4), name VARCHAR, place VARCHAR, mobile VARCHAR)")

            var cursor = database.rawQuery("select * from employees",null)

            val idIndex = cursor.getColumnIndex("id")
            val nameIndex = cursor.getColumnIndex("name")
            val placeIndex = cursor.getColumnIndex("place")
            val mobileIndex = cursor.getColumnIndex("mobile")

            cursor.moveToFirst()

            while (cursor != null){
                EmployeeIdArray.add(cursor.getInt((idIndex)))
                EmployeeNameArray.add(cursor.getString(nameIndex))
                EmployeePlaceArray.add(cursor.getString(placeIndex))
                EmployeeMobileArray.add(cursor.getString(mobileIndex))
                cursor.moveToNext()
            }
            cursor?.close()
        } catch (e: java.lang.Exception){
            e.printStackTrace()
        }
        var visszateres:Boolean = false
        for (elem in EmployeeIdArray){
            if (elem == new_id) {
                visszateres=true
            }
        }
        return visszateres
    }
}
