package com.example.myasset

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.AdapterView
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.activity_addemployee.*
import kotlinx.android.synthetic.main.activity_employee_profile.*
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.Exception

class EmployeeProfile : AppCompatActivity() {

    var actual_eid: Int = 0
    var actual_ename = ""
    var actual_eplace = ""
    var actual_emobile = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_employee_profile)

        var eid = intent.getIntExtra("id",0)
        var ename = ""
        var eplace = ""
        var emobile = ""

        val database = this.openOrCreateDatabase("AssetDB", Context.MODE_PRIVATE, null)
        val selectQuery = "SELECT  * FROM employees WHERE id= $eid"
        database.rawQuery(selectQuery, null).use {
            if (it.moveToFirst()) {
                ename = it.getString(it.getColumnIndex("name"))
                eplace = it.getString(it.getColumnIndex("place"))
                emobile = it.getString(it.getColumnIndex("mobile"))
            }
        }

        actual_eid = eid
        actual_ename = ename
        actual_eplace = eplace
        actual_emobile = emobile

        profile_id_Num.setText(eid.toString())
        profile_Name_Text.setText(ename)
        profile_workplace_Text.setText(eplace)
        profile_mobile_Text.setText(emobile)

        val assetCompany = ArrayList<String>()
        val assetType = ArrayList<String>()
        val assetFull = ArrayList<String>()
        val arrayAdapter = ArrayAdapter(this,android.R.layout.simple_list_item_1,assetFull)
        user_asset_list.adapter = arrayAdapter
        try {
            val database = this.openOrCreateDatabase("AssetDB", Context.MODE_PRIVATE, null)
            database.execSQL("CREATE TABLE IF NOT EXISTS assets (eid INTEGER(4), company VARCHAR, type VARCHAR)")
            val cursor = database.rawQuery("SELECT * FROM assets where eid=$actual_eid", null)
            val idIndex = cursor.getColumnIndex("eid")
            val companyIndex = cursor.getColumnIndex("company")
            val typeIndex = cursor.getColumnIndex("type")
            cursor.moveToFirst()
            while (cursor != null){
                assetCompany.add(cursor.getString(companyIndex))
                assetType.add(cursor.getString(typeIndex))
                var full_str = "${cursor.getString(companyIndex)} - ${cursor.getString(typeIndex)}"
                assetFull.add(full_str)
                cursor.moveToNext()
                arrayAdapter.notifyDataSetChanged()
            }
            cursor?.close()
        } catch (e: Exception){
            e.printStackTrace()
        }
    }
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val menuInflater = menuInflater
        menuInflater.inflate(R.menu.employee_menu,menu)

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        if(item?.itemId == R.id.add_asset){
            val intent = Intent(applicationContext, AddAsset::class.java)
            intent.putExtra("id",actual_eid)
            intent.putExtra("name", actual_ename)
            intent.putExtra("place", actual_eplace)
            intent.putExtra("mobile", actual_emobile)
            startActivity(intent)
        } else if(item?.itemId == R.id.remove_asset){
            val database = this.openOrCreateDatabase("AssetDB", Context.MODE_PRIVATE, null)
            database.execSQL("DELETE FROM assets where eid=$actual_eid")
            finish()
            startActivity(getIntent())
        } else if(item?.itemId == R.id.delete_user){
            val database = this.openOrCreateDatabase("AssetDB", Context.MODE_PRIVATE, null)
            database.execSQL("DELETE FROM assets where eid=$actual_eid")
            database.execSQL("DELETE FROM employees where id=$actual_eid")
            val intent = Intent(applicationContext, MainActivity::class.java)
            startActivity(intent)
        } else if(item?.itemId == R.id.save_user_changes){
            val database = this.openOrCreateDatabase("AssetDB", Context.MODE_PRIVATE, null)
            val new_name = profile_Name_Text.text.toString()
            val new_place = profile_workplace_Text.text.toString()
            val new_mobile = profile_mobile_Text.text.toString()
            database.execSQL("update employees set name='$new_name', place='$new_place', mobile='$new_mobile' where id=$actual_eid")
            finish()
            startActivity(getIntent())
        }
        return super.onOptionsItemSelected(item)
    }
}
