package com.example.myasset

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_add_asset.*
import kotlinx.android.synthetic.main.activity_addemployee.*

class AddAsset : AppCompatActivity() {

    var employee_id : Int = 0
    var actual_ename = ""
    var actual_eplace = ""
    var actual_emobile = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        employee_id = intent.getIntExtra("id",0)
        actual_ename = intent.getStringExtra("name")
        actual_eplace = intent.getStringExtra("place")
        actual_emobile = intent.getStringExtra("mobile")
        setContentView(R.layout.activity_add_asset)

    }

    fun addassetButtonClicked(view: View){
        val company = companyText.text.toString()
        val type =  typeText.text.toString()
        try {
            val database = this.openOrCreateDatabase("AssetDB", Context.MODE_PRIVATE, null)
            database.execSQL("CREATE TABLE IF NOT EXISTS assets (eid INTEGER(4), company VARCHAR, type VARCHAR)")
            var insertSQL = "INSERT INTO assets (eid, company, type) VALUES ($employee_id,'$company','$type')"
            database.execSQL(insertSQL)
        } catch (e: Exception){
            e.printStackTrace()
        }

        val intent = Intent(applicationContext, EmployeeProfile::class.java)
        intent.putExtra("id", employee_id)
        intent.putExtra("name", actual_ename)
        intent.putExtra("place", actual_eplace)
        intent.putExtra("mobile", actual_emobile)
        startActivity(intent)
    }
}
