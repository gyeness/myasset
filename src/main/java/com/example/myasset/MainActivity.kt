package com.example.myasset

import android.content.Context
import android.content.Intent
import android.database.sqlite.SQLiteDatabase
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.Exception

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val EmployeeIdArray = ArrayList<Int>()
        val EmployeeNameArray = ArrayList<String>()
        val EmployeePlaceArray = ArrayList<String>()
        val EmployeeMobileArray = ArrayList<String>()

        val arrayAdapter = ArrayAdapter(this,android.R.layout.simple_list_item_1,EmployeeNameArray)
        listView.adapter = arrayAdapter

        try {
            val database = this.openOrCreateDatabase("AssetDB", Context.MODE_PRIVATE, null)
            database.execSQL("CREATE TABLE IF NOT EXISTS employees (id INTEGER(4), name VARCHAR, place VARCHAR, mobile VARCHAR)")
            val cursor = database.rawQuery("SELECT * FROM employees", null)
            val idIndex = cursor.getColumnIndex("id")
            val nameIndex = cursor.getColumnIndex("name")
            val placeIndex = cursor.getColumnIndex("place")
            val mobileIndex = cursor.getColumnIndex("mobile")
            cursor.moveToFirst()

            while (cursor != null){
                EmployeeIdArray.add(cursor.getInt((idIndex)))
                EmployeeNameArray.add(cursor.getString(nameIndex))
                EmployeePlaceArray.add(cursor.getString(placeIndex))
                EmployeeMobileArray.add(cursor.getString(mobileIndex))
                cursor.moveToNext()
                arrayAdapter.notifyDataSetChanged()
            }
            cursor?.close()
        } catch (e: Exception){
            e.printStackTrace()
        }
        listView.onItemClickListener = AdapterView.OnItemClickListener{adapterView, view, i ,l ->
            val intent = Intent(applicationContext,EmployeeProfile::class.java)
            intent.putExtra("id", EmployeeIdArray[i])
            startActivity(intent)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val menuInflater = menuInflater
        menuInflater.inflate(R.menu.add_employee,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item?.itemId == R.id.add_employee){
            val intent = Intent(applicationContext, AddEmployeeActivity::class.java)
            startActivity(intent)
        } else if(item?.itemId == R.id.truncate_database){
            val database = this.openOrCreateDatabase("AssetDB", Context.MODE_PRIVATE, null)
            database.execSQL("DELETE FROM employees where 1=1")
            database.execSQL("delete from assets where 1=1")
            finish()
            startActivity(getIntent())
        }
        return super.onOptionsItemSelected(item)
    }

    fun search_user_Button_Clicked(view: View){
        val EmployeeIdArray = ArrayList<Int>()
        val EmployeeNameArray = ArrayList<String>()
        val EmployeePlaceArray = ArrayList<String>()
        val EmployeeMobileArray = ArrayList<String>()
        var user_to_search = ""
        user_to_search = search_user_Text.text.toString()
        val arrayAdapter = ArrayAdapter(this,android.R.layout.simple_list_item_1,EmployeeNameArray)
        listView.adapter = arrayAdapter
        try {
            val database = this.openOrCreateDatabase("AssetDB", Context.MODE_PRIVATE, null)
            database.execSQL("CREATE TABLE IF NOT EXISTS employees (id INTEGER(4), name VARCHAR, place VARCHAR, mobile VARCHAR)")
            var cursor = database.rawQuery("select * from employees",null)
            if(user_to_search != ""){
                cursor = database.rawQuery("SELECT * FROM employees where lower(name) LIKE lower('%$user_to_search%')", null)
            } else if(user_to_search == ""){
                cursor = database.rawQuery("SELECT * FROM employees", null)
            }
            val idIndex = cursor.getColumnIndex("id")
            val nameIndex = cursor.getColumnIndex("name")
            val placeIndex = cursor.getColumnIndex("place")
            val mobileIndex = cursor.getColumnIndex("mobile")
            cursor.moveToFirst()
            while (cursor != null){
                EmployeeIdArray.add(cursor.getInt(idIndex))
                EmployeeNameArray.add(cursor.getString(nameIndex))
                EmployeePlaceArray.add(cursor.getString(placeIndex))
                EmployeeMobileArray.add(cursor.getString(mobileIndex))
                cursor.moveToNext()
                arrayAdapter.notifyDataSetChanged()
            }
            cursor?.close()
        } catch (e: Exception){
            e.printStackTrace()
        }
        listView.onItemClickListener = AdapterView.OnItemClickListener{adapterView, view, i ,l ->
            val intent = Intent(applicationContext,EmployeeProfile::class.java)
            intent.putExtra("id", EmployeeIdArray[i])
            startActivity(intent)
        }
    }
}
